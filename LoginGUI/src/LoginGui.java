import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginGui extends JFrame {

    public LoginGui(){
        setTitle("Login");
        setSize(380,230);
        setLayout(null);

        UserDataAccessObject userDao = new UserDataAccessObject("localhost","3306","login_gui_db","root","");

        JLabel lbluser = new JLabel("User");
        lbluser.setBounds(10,10,150,25);
        add(lbluser);

        JLabel lblpassword = new JLabel("Password");
        lblpassword.setBounds(10,60,150,25);
        add(lblpassword);

        JTextField user = new JTextField();
        user.setBounds(160,10,150,25);
        add(user);

        JPasswordField password = new JPasswordField();
        password.setBounds(160,60,150,25);
        add(password);

        JButton login = new JButton("Login");
        login.setBounds(10,120,100,25);
        add(login);
        login.addActionListener(e -> {
            System.out.println("Login: "+userDao.login(user.getText(),new String(password.getPassword())));
            System.out.println("Secure Login: "+userDao.secureLogin(user.getText(),new String(password.getPassword())));
        });

        JButton registration = new JButton("Registrieren");
        registration.setBounds(25,160,125,25);
        add(registration);

        JButton forgotPassword = new JButton("Password vergessen");
        forgotPassword.setBounds(160,160,175,25);
        add(forgotPassword);

        setResizable(true);
        setVisible(true);
    }
}
