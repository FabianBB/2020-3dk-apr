import javax.swing.*;

public class PingPongGui extends JFrame {

    public PingPongGui(int noOTBalls){
        super("PingPong");

        PingPongPanel canvas = new PingPongPanel();

        setContentPane(canvas);
        setLocationRelativeTo(null);
        setSize(500,400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        canvas.initBar();

        //Die Umgebungsvariable hat einen Wert von 2
        for (int i = 0; i < Integer.parseInt(System.getenv("PINGPONGGAME")); i++) {
            PingPongBall ball = new PingPongBall(i * 20 + 10,i * 20 + 10, canvas, canvas.getBar());
            ball.start();
        }
    }
}
