import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BallCreatingMouseAdapter extends MouseAdapter{

    private PingPongPanel pingPongPanel;
    private final PingPongBar bar;

    public BallCreatingMouseAdapter(PingPongPanel pingPongPanel, PingPongBar bar) {
        this.pingPongPanel = pingPongPanel;
        this.bar = bar;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        PingPongBall ball = new PingPongBall(e.getX(),e.getY(),this.pingPongPanel,this.bar);
        ball.start();
    }
}
