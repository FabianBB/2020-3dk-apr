public class ThreadMain {
    public static void main(String[] args) {
        System.out.println("Start");

        MyThread t1 = new MyThread("Max", 2000);
        t1.start();

        MyThread t2 = new MyThread("Karl", 500);
        t2.start();

        Thread t3 = new Thread(() -> System.out.println("My Runnable"));
        t3.start();

        System.out.println("Ende");
    }
}
