import javax.swing.*;
import java.awt.*;

public class GaesteregistrierungFields extends JPanel {

    private final JTextField txtvn = new JTextField();
    private final JTextField txtnn = new JTextField();
    private final JTextField txtst = new JTextField();
    private final JTextField txtpl = new JTextField();
    private final JTextField txtor = new JTextField();
    private final JTextField txtte = new JTextField();
    private final JTextField txtem = new JTextField();
    private final JTextField txtti = new JTextField();

    public GaesteregistrierungFields() {
        setPreferredSize(new Dimension(550,160));
        setLayout(null);

        GuestRegistrationValidator grv = new GuestRegistrationValidator();

        JLabel vorname = new JLabel("Vorname:");
        JLabel nachname = new JLabel("Nachname:");
        JLabel strasse = new JLabel("Straße:");
        JLabel plz = new JLabel("PLZ:");
        JLabel ort = new JLabel("Ort:");

        JLabel telnum = new JLabel("Telefonnummer:");
        JLabel email = new JLabel("E-Mail:");
        JLabel tischnr = new JLabel("Tisch-Nr.:");

        vorname.setBounds(20,0,100,25);
        nachname.setBounds(20,25,100,25);
        strasse.setBounds(20,50,100,25);
        plz.setBounds(20,75,100,25);
        ort.setBounds(20,100,100,25);
        telnum.setBounds(275,0,100,25);
        email.setBounds(275,25,100,25);
        tischnr.setBounds(275,50,100,25);

        add(vorname);
        add(nachname);
        add(strasse);
        add(plz);
        add(ort);
        add(telnum);
        add(email);
        add(tischnr);

        txtvn.setBounds(120,3,100,18);
        txtnn.setBounds(120,28,100,18);
        txtst.setBounds(120,53,100,18);
        txtpl.setBounds(120,78,40,18);
        txtor.setBounds(120,103,100,18);
        txtte.setBounds(400,3,100,18);
        txtem.setBounds(400,28,100,18);
        txtti.setBounds(400,53,100,18);

        add(txtvn);
        add(txtnn);
        add(txtst);
        add(txtpl);
        add(txtor);
        add(txtte);
        add(txtem);
        add(txtti);

        JButton save = new JButton("Speichern");
        save.setBounds(125,135,150,25);
        save.addActionListener(e -> {
            try {
                grv.checkValues(this);
            } catch (GuestDataNotValidExeption guestDataNotValidExeption) {
                guestDataNotValidExeption.getErrors();
            }
        });

        JButton reset = new JButton("Zurücksetzen");
        reset.setBounds(260,135,150,25);
        reset.addActionListener(e -> {
            txtvn.setText(null);
            txtnn.setText(null);
            txtst.setText(null);
            txtpl.setText(null);
            txtor.setText(null);
            txtte.setText(null);
            txtem.setText(null);
            txtti.setText(null);
        });

        add(save);
        add(reset);

    }

    public String getTxtvn() {
        return txtvn.getText();
    }
    public String getTxtnn() {
        return txtnn.getText();
    }
    public String getTxtte(){
        return txtte.getText();
    }
    public String getTxtem(){
        return txtem.getText();
    }
    public String getTxtti(){
        return txtti.getText();
    }

}