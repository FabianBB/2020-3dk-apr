import java.util.ArrayList;

public class GuestDataNotValidExeption extends Exception{

    private final ArrayList<String> errorList = new ArrayList<>();

    public GuestDataNotValidExeption() {

    }

    public void addErrorMessage(String errorMessage){
        this.errorList.add(errorMessage);
    }

    public ArrayList<String> getErrorList() {
        return errorList;
    }

    // printErrors() wäre ein besserer Name
    public void getErrors() {
        for (String s: errorList) {
            System.err.println(s);
        }
    }
}
