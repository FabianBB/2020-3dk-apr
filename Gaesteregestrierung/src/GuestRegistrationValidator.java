// AVK3: 11 Punkte
// Sehr gute Arbeit

public class GuestRegistrationValidator {

    // methode sollte validate heißen (siehe Angabe)
    public void checkValues(GaesteregistrierungFields grf) throws GuestDataNotValidExeption{

        GuestDataNotValidExeption exeption = new GuestDataNotValidExeption();

        try{ //checking Vorname
            checkEmpty(grf.getTxtvn());
            checkMaxLength(grf.getTxtvn(),15);
        } catch (IllegalArgumentException e) {
            exeption.addErrorMessage("First name must not be empty or longer than 15 characters");
        }

        try{ //checking Nachname
            checkEmpty(grf.getTxtnn());
            checkMaxLength(grf.getTxtnn(),20);
        } catch (IllegalArgumentException e) {
            exeption.addErrorMessage("Last name must not be empty or longer than 20 characters");
        }

        try{ //checking for Max Mustermann
            checkMaxMustermann(grf.getTxtvn(), grf.getTxtnn());
        } catch (IllegalArgumentException e) {
            exeption.addErrorMessage("Max Mustermann is not allowed");
        }

        try{ //checking Tischnummer
            checkEmpty(grf.getTxtti());
            checkInteger(grf.getTxtti());
            checkTablenum(grf.getTxtti());
        } catch (IllegalArgumentException e) {
            exeption.addErrorMessage("Table number must not be empty or the number does not exist");
        }

        try{ //checking Email and Telefonnummer
            checkEmpty(grf.getTxtem());
            checkEmpty(grf.getTxtte());
        } catch (IllegalArgumentException e) {
            exeption.addErrorMessage("Email or phone number must be provided");
        }

        //Getting Error Messages
        if (!exeption.getErrorList().isEmpty()){
            throw exeption;
        }

    }

    // die throws-Klausel is hier nicht notwendig bzw. sogar falsch: -1
    private void checkEmpty(String value) throws GuestDataNotValidExeption{
        if (value == null || value.isEmpty()){
            throw new IllegalArgumentException();
        }
    }

    private void checkMaxLength(String value, int maxLength) throws GuestDataNotValidExeption{
        if (value.length() > maxLength){
            throw new IllegalArgumentException();
        }
    }

    private void checkMaxMustermann(String value1, String value2) throws GuestDataNotValidExeption{
        if (value1.equals("Max") && value2.equals("Mustermann")){
            throw new IllegalArgumentException();
        }
    }

    private void checkInteger(String value) throws GuestDataNotValidExeption {
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

    private void checkTablenum(String value) throws GuestDataNotValidExeption{
        if (Integer.parseInt(value) < 0 && Integer.parseInt(value) > 100){
            throw new IllegalArgumentException();
        }
    }
}