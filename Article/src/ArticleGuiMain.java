import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ArticleGuiMain {

    public static void main(String[] args) throws IOException {

        Properties properties = new Properties();
        FileInputStream in = new FileInputStream("database.properties");
        properties.load(in);
        in.close();

	    new Articlemanagement(properties.get("host").toString(),properties.get("port").toString(),properties.get("name").toString(),properties.get("user").toString());
    }
}
