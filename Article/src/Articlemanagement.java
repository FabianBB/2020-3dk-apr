import javax.swing.*;
import java.awt.*;

public class Articlemanagement extends JFrame {

    public Articlemanagement(String host,String port,String name,String user){
        super("Artikelverwaltung");
        setSize(450,510);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        ArticleDataAccessObject dao = new ArticleDataAccessObject(host,port,name,user,"");

        ArticleTableModel tableModel = new ArticleTableModel();
        tableModel.initData(dao.readAllArticles());

        UpperPanel upperPanel = new UpperPanel(dao,tableModel);
        add(upperPanel,BorderLayout.NORTH);
        add(new LowerPanel(dao,tableModel, upperPanel),BorderLayout.SOUTH);

        setVisible(true);
    }
}
