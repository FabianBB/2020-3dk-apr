import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDataAccessObject {

    private final String user;
    private final String password;

    private final String databaseurl;

    public ArticleDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.databaseurl = "jdbc:mariadb://"+ host +":"+ port +"/"+database;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    private Connection connect(){
        try {
            return DriverManager.getConnection(this.databaseurl,this.user,this.password);
        } catch (SQLException throwables){
            throw new RuntimeException();
        }
    }

    private void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.out.println("Closed connection");
        }
    }

    public List<Article> readAllArticles(){
        Connection connection = connect();
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = "select * from article";
            ResultSet resultSet = statement.executeQuery(sql);
            return extractArticles(resultSet);
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        }  finally {
            close(connection);
        }
    }

    public Article read(int id){
        Connection connection = connect();
        String sql = "select * from article where id = "+id;
        Statement select = null;
        try {
            select = connect().createStatement();
            ResultSet resultSet = select.executeQuery(sql);
            List<Article> articles = extractArticles(resultSet);
            if (articles.get(0)!=null){
                return articles.get(0);
            }
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement"+throwables);
        } finally {
            close(connection);
        }
        return null;
    }

    public void insert(Article article){
        Connection connection = connect();
        String sql = "insert into article(description,price,stock) VALUES ('" + article.getDescription() + "','" + article.getPrice() + "','" + article.getStock() + "')";
        Statement insert = null;
        try {
            insert = connection.createStatement();
            insert.execute(sql);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statment", throwables);
        } finally {
            close(connection);
        }
        System.out.println("Insert Execute successfully");
    }

    public void update(Article article){
        Connection connection = connect();
        String sql = "update article set description='"+article.getDescription()+"', price="+article.getPrice()+", stock="+article.getStock()+" where id="+article.getId();
        Statement update = null;
        try{
            update = connect().createStatement();
            update.execute(sql);
            System.out.println("Update executed successfully!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally{
            close(connection);
        }
    }

    public void delete(int id){
        Connection connection = connect();
        String sql = "delete from article where id="+id;
        Statement delete = null;
        try{
            delete = connect().createStatement();
            delete.execute(sql);
            System.out.println("Deleted successfully!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally{
            close(connection);
        }
    }

    public List<Article> findBy(String description){
        Connection connection = connect();
        String sql = "select * from article where lower(description) like '%"+description+"%'";
        Statement findBy = null;

        try {
            findBy = connection.createStatement();
            ResultSet resultSet = findBy.executeQuery(sql);
            return extractArticles(resultSet);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement"+throwables);
        } finally {
            close(connection);
        }
    }

    private ArrayList<Article> extractArticles(ResultSet resultSet) throws SQLException {
        ArrayList<Article> article = new ArrayList<>();
        while (resultSet.next()) {
            article.add(new Article(resultSet.getInt(1), resultSet.getString(2), resultSet.getDouble(3), resultSet.getInt(4)));
        }
        return article;
    }
}
