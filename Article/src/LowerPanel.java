import javax.swing.*;
import java.awt.*;

public class LowerPanel extends JPanel {

    public LowerPanel(ArticleDataAccessObject dao, ArticleTableModel tableModel, UpperPanel upperPanel) {
        setPreferredSize(new Dimension(450,250));
        setLayout(new FlowLayout());

        JTable jTable = new JTable(tableModel);
        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSelectionModel = jTable.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ArticleListSelectionListener(jTable, upperPanel));

        JScrollPane scrollPane = new JScrollPane(jTable);

        scrollPane.setPreferredSize(new Dimension(400,150));
        scrollPane.setVerticalScrollBarPolicy(scrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(400,75));
        panel.setLayout(null);

        JButton showArticle = new JButton("zeige Artikelliste");
        showArticle.setBounds(0,5,125,25);
        showArticle.addActionListener(e -> tableModel.initData(dao.readAllArticles()));

        JButton exit = new JButton("beenden");
        exit.setBounds(275,5,125,25);
        exit.addActionListener(e -> System.exit(0));

        panel.add(showArticle);
        panel.add(exit);

        add(panel);
    }
}