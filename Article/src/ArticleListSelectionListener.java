import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ArticleListSelectionListener implements ListSelectionListener {

    private JTable table;
    private UpperPanel upperPanel;

    public ArticleListSelectionListener(JTable jTable, UpperPanel upperPanel) {
        this.table = jTable;
        this.upperPanel = upperPanel;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()){
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
        int selectedRow = lsm.getMinSelectionIndex();

        if( selectedRow >= 0 ){
            int id = (int) table.getValueAt(selectedRow,0);
            upperPanel.loadArticle(id);
        }
    }
}
