import javax.swing.*;
import java.awt.*;
import java.util.List;

public class UpperPanel extends JPanel {

    private final JTextField txtarticlenum = new JTextField();
    private final JTextField txtarticlename = new JTextField();
    private final JTextField txtarticleprice = new JTextField();
    private final JTextField txtarticleamount = new JTextField();

    private ArticleDataAccessObject dao;
    private ArticleTableModel tableModel;

    public UpperPanel(ArticleDataAccessObject dao, ArticleTableModel tableModel) {
        setPreferredSize(new Dimension(450,220));
        setLayout(null);

        this.dao = dao;
        this.tableModel = tableModel;

        /*-- Die Labels --*/
        JLabel articlelbl = new JLabel("Artikel");
        JLabel articlenum = new JLabel("Artikelnummer:");
        JLabel articlename = new JLabel("Artikelname:");
        JLabel articleprice = new JLabel("Artikelpreis:");
        JLabel articleamount = new JLabel("Bestand:");

        articlelbl.setFont(new Font("Segoe UI",Font.BOLD,18));

        articlelbl.setBounds(25,25,75,25);
        articlenum.setBounds(25,75,125,25);
        articlename.setBounds(25,110,125,25);
        articleprice.setBounds(25,145,125,25);
        articleamount.setBounds(25,180,125,25);

        add(articlelbl);
        add(articlenum);
        add(articlename);
        add(articleprice);
        add(articleamount);

        /*-- Die TextFields --*/
        txtarticlenum.setBounds(150,75,100,25);
        txtarticlename.setBounds(150,110,100,25);
        txtarticleprice.setBounds(150,145,100,25);
        txtarticleamount.setBounds(150,180,100,25);

        txtarticlenum.setEditable(false);
        txtarticlenum.setText("0");

        add(txtarticlenum);
        add(txtarticlename);
        add(txtarticleprice);
        add(txtarticleamount);

        /*-- Die Buttons --*/
        JButton neu = new JButton("Neu");
        neu.setBounds(300,25,100,25);
        JButton create = new JButton("Anlegen");
        create.setBounds(300,75,100,25);
        JButton change = new JButton("Ändern");
        change.setBounds(300,110,100,25);
        JButton delete = new JButton("Löschen");
        delete.setBounds(300,145,100,25);
        JButton search = new JButton("Suchen");
        search.setBounds(300,180,100,25);

        neu.addActionListener(e -> {
            txtarticlenum.setText("0");
            txtarticlename.setText("");
            txtarticleprice.setText("");
            txtarticleamount.setText("");
        });
        create.addActionListener(e -> {
            dao.insert(createArticle());
            updatingTable();
        });
        change.addActionListener(e -> {
            dao.update(createArticle());
            updatingTable();
        });
        delete.addActionListener(e -> {
            dao.delete(Integer.parseInt(txtarticlenum.getText()));
            updatingTable();
        });
        search.addActionListener(e -> {
            String desc = txtarticlename.getText();
            List<Article> articles = dao.findBy(desc);;
            tableModel.initData(articles);
        });

        add(neu);
        add(create);
        add(change);
        add(delete);
        add(search);
    }

    private Article createArticle(){
        Article article = new Article(checkInteger(checkEmpty(txtarticlenum.getText())), checkMaxLength(checkEmpty(txtarticlename.getText()),15), checkDouble(checkEmpty(txtarticleprice.getText())), checkInteger(checkEmpty(txtarticleamount.getText())));
        return article;
    }

    private void updatingTable(){
        List<Article> articles = dao.readAllArticles();
        tableModel.initData(articles);
    }

    private String checkEmpty(String value){
        if (value == null || value.isEmpty()){
            throw new IllegalArgumentException("Eingabe darf nicht leer sein!!");
        } else {
            return value;
        }
    }

    private String checkMaxLength(String value, int maxLength){
        if (value.length() > maxLength){
            throw new IllegalArgumentException("Die Beschreibung darf nicht länger als 15 Zeichen haben!!");
        } else {
            return value;
        }
    }

    private int checkInteger(String value){
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Der eingebene Wert representiert keine Zahl!!");
        }
    }

    private double checkDouble(String value){
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Der eingebene Wert kann keine Dezimalstelle haben!!");
        }
    }

    public void loadArticle(int id){
        Article artilce = dao.read(id);
        txtarticlenum.setText(artilce.getId()+"");
        txtarticlename.setText(artilce.getDescription());
        txtarticleprice.setText(artilce.getPrice()+"");
        txtarticleamount.setText(artilce.getStock()+"");
    }
}