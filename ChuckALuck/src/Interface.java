import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame {

    private final String[] ARR = new String[]{"lblDice1", "lblDice2","lblDice3"};

    private JPanel heading = new JPanel();
    private JLabel lblHeading = new JLabel("CHUCK A LUCK");

    private Bankaccount myAccount = new Bankaccount();
    private SetNumber myNumber = new SetNumber();
    private ThrowDice myDice = new ThrowDice();

    public Interface(String title) {
        super(title);
        add(myAccount, BorderLayout.WEST);
        add(myNumber, BorderLayout.CENTER);
        add(myDice, BorderLayout.EAST);

        new Game(myNumber,myDice,myAccount);

        heading.setBackground(Color.BLUE);

        heading.add(lblHeading);
        lblHeading.setForeground(Color.WHITE);
        lblHeading.setFont(new Font("Times New Roman", Font.BOLD, 18));
        lblHeading.setHorizontalAlignment(SwingConstants.CENTER);
        add(heading, BorderLayout.NORTH);
    }
}