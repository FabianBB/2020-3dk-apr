public class Message {

    private int id;
    private String message;
    private String language;

    public Message(int id, String message,String language) {
        this.id = id;
        this.message = message;
        this.language = language;
    }

    @Override
    public String toString() {
        return id + " | " + message + " | " + language;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getLanguage() {
        return language;
    }
}
