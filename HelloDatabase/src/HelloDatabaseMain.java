import java.util.List;

/*
* Falls Hü kommt dann wird sie hier eingetragen:
*
* */

public class HelloDatabaseMain {

    public static void main(String[] args){

        String dbVendor = null;
        if(args.length == 1 && args[0].equals("oracle")){
            dbVendor = "oracle";
        } else {
            dbVendor = "mariadb"; //use "postgres" to get an error
        }


        MessageDataAccessObject messageDAO = new MessageDataAccessObject("localhost","3306","hello_world","root","",dbVendor);

        System.out.println(messageDAO.read(2));

        Message message = new Message(2,"Selam Dünya ","ty");
        messageDAO.update(message);

        System.out.println(messageDAO.read(2));

        //messageDAO.delete(6);

        //List<Message> messages = messageDAO.readAllMessages();
        //messageDAO.insert("Bonjour le monde ","fr");
        //messages.forEach(System.out::println); //das ist eine for each schleife in neu
    }
}