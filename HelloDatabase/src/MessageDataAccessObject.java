import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDataAccessObject {

    private DataBaseAccess dataBaseAccess;

    public MessageDataAccessObject(String host, String port, String database, String user, String password,String dbVendor) {
        /*
        if ("oracle".equals(dbVendor)){
            dataBaseAccess = new OracleDataBaseAccess(host,port,database,user,password);
        } else {
            dataBaseAccess = new MariadbDataBaseAccess(host,port,database,user,password);
        }*/

        try {
            dataBaseAccess = new DataBaseAccess(host,port,database,user,password,dbVendor);
        } catch (DbVendorNotSupportedExeption e) {
            System.err.println("Could not start program!");
        }
    }

    private Connection connect(){
        return dataBaseAccess.connect();
    }

    private void close(Connection connection){
        dataBaseAccess.close(connection);
    }

    public void insert(String text, String language){
        Connection connection = connect();
        String sql = "insert into message(text,language) VALUES (?,?)";
        Statement insert = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,text);
            preparedStatement.setString(2,language);
            preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statment", throwables);
        } finally {
            close(connection);
        }
        System.out.println("Insert Execute successfully");
    }

    public void delete(int id){
        Connection connection = connect();
        Statement statement = null;
        String sql = "DELETE FROM message WHERE id = ?";

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            preparedStatement.executeQuery();
            System.out.println("Deleted Element on ID: "+id);
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        } finally {
            close(connection);
            System.out.println("Closed Succesfully");
        }
    }

    public void update(Message message){
        Connection connection = connect();
        Statement statement = null;

        String sql = "update message set text = ?, language = ? Where id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,message.getMessage());
            preparedStatement.setString(2,message.getLanguage());
            preparedStatement.setInt(3,message.getId());
            preparedStatement.executeQuery();
            System.out.println("Updated at ID: "+message.getId());
        } catch(SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
            System.out.println("Closed Succesfully");
        }
    }

    public Message read(int id){
        Message message = null;

        Connection connection = connect();
        System.out.println("Insert Execute successfully");
        Statement statement = null;
        String sql = "select * from message where id = ?";

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                message = new Message(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3));
            }
        } catch(SQLException throwables){
            System.out.println("Could not create statement");
            throwables.printStackTrace();
        } finally {
            close(connection);
            System.out.println("Closed Succesfully");
        }

        return message;
    }

    public List<Message> readAllMessages(){
        Connection connection = connect();
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = "select * from message";
            ResultSet resultSet;
            resultSet = statement.executeQuery(sql);
            ArrayList<Message> messages = new ArrayList<>();
            while(resultSet.next()){
                messages.add(new Message(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3)));
            }
            return messages;
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        } finally {
            close(connection);
        }
    }
}
