public class FrenchPerson extends Person {

    public FrenchPerson(String name) {
        super(name, "französisch");
    }

    @Override
    public String greet() {
        return "Bon jour " + name;
    }
}
