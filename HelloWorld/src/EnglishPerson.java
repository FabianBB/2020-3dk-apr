public class EnglishPerson extends Person {

    public EnglishPerson(String name) {
        super(name, "englisch");
    }

    @Override
    public String greet() {
        return "Hello " + this.name + "!";
    }
}
