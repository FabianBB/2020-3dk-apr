import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

public class HelloWorldMain {

    public static void main(String[] args) throws IOException {

        handleProgamArguments(args);
        System.out.println("-------------");
        handlePropertiesFile();
        System.out.println("-------------");
        handleProperties();
        System.out.println("-------------");
        handleEnviromentVariables();

        /*EnglishPerson englishPerson = new EnglishPerson("Johann");

        SpanishPerson spanishPerson = new SpanishPerson("Johann");

        GermanPerson germanPerson = new GermanPerson("Johann");

        FrenchPerson frenchPerson = new FrenchPerson("Johann");

        AustrianPerson austrianPerson = new AustrianPerson("Johann");

        ArrayList<Person> persons = new ArrayList<>();
        persons.add(englishPerson);
        persons.add(spanishPerson);
        persons.add(germanPerson);
        persons.add(frenchPerson);
        persons.add(austrianPerson);

        new HelloWorldGui("Hello World", persons);*/
    }

    private static void handleEnviromentVariables() {
        /*
         *{
         * "JAVA_HOME" = ".............."
         * "KONFIG" = "............."
         * "PATH" = "................"
         *}
         */
        System.out.println("Read specific enviroment variable");
        System.out.println("KONFIG: " + System.getenv("KONFIG"));

        Map<String,String> env = System.getenv();
        for (Map.Entry<String,String> entry: env.entrySet()) {
            System.out.println("Variable Name: "+entry.getKey() + ", Value: " + entry.getValue());
        }
    }

    private static void handlePropertiesFile() throws IOException {
        Properties properties = new Properties();
        FileInputStream in = new FileInputStream("config.properties");
        properties.load(in);
        in.close();
        System.out.println("host="+properties.get("host"));
        System.out.println("port="+properties.get("port"));
    }

    private static void handleProperties(){
        String host = System.getProperty("host");
        System.out.println("host="+host);
        String port = System.getProperty("port");
        System.out.println("port="+port);
    }

    private static void handleProgamArguments(String[] args){
        System.out.println("Hello!");
        for (String s : args) {
            System.out.println(s);
        }

        if (args.length == 0){
            System.err.println("Mindestens 1 agrument muss übergeben weredn");
            System.exit(-1);
        }
        System.out.println(args[0]);
    }
}
