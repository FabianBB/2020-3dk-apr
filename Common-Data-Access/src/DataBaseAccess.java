import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseAccess {

    private final String user;
    private final String password;

    private String connectionUrl;

    public DataBaseAccess(String host, String port, String dbName, String user, String password, String dbVendor) throws DbVendorNotSupportedExeption {
        this.user = user;
        this.password = password;
        if ("mariadb".equals(dbVendor)){
            initMariadb(host,port,dbName);
        } else if ("oracle".equals(dbVendor)){
            initOrcaleDb(host, port, dbName);
        }
        //throw new RuntimeException(dbVendor + "is currently not support. Use orcale or mariadb!");
        throw new DbVendorNotSupportedExeption(dbVendor + "is not supported!");
    }

    private void initOrcaleDb(String host, String port, String dbName) {
        this.connectionUrl = "jdbc:oracle://"+ host +":"+ port +"/"+ dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    private void initMariadb(String host,String port,String dbName){
        this.connectionUrl = "jdbc:mariadb://"+ host +":"+ port +"/"+ dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    public Connection connect(){
        System.out.println("Connecting to database");
        try {
            return DriverManager.getConnection(this.connectionUrl,this.user,this.password);
        } catch (SQLException throwables){
            throw new RuntimeException("Could not connect to database",throwables);
        }
    }

    public void close(Connection connection){
        System.out.println("Closing connection");
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection: " + throwables.getMessage());
        }
    }
}
