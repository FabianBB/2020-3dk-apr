public class DbVendorNotSupportedExeption extends Exception{

    public DbVendorNotSupportedExeption (String message){
        super(message);
    }

}
