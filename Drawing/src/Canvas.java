import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {

    public Canvas() {
        setBackground(Color.BLACK);
        setForeground(Color.MAGENTA);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawLine(10,10,50,50);
        g.drawString("Test Text",50,50);
    }

}
