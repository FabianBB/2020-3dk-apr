package f.breuer;

import javax.swing.*;

public class MouseClickCountingGui extends JFrame {

    public MouseClickCountingGui(){
        setSize(450,400);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        MouseClickCounting mcc = new MouseClickCounting();
        addMouseListener(mcc);

        JLabel label = new JLabel("0");
        label.setBounds(175,65,100,25);
        label.setVerticalTextPosition(JLabel.CENTER);
        add(label);

        JButton button = new JButton("Button");
        button.setBounds(175,20,100,25);
        button.addActionListener(e -> label.setText(""+mcc.getClicks()));
        add(button);

        setVisible(true);
    }
}
