import javax.swing.*;
import java.awt.*;

public class Funktionsplotter extends JFrame {

    public Funktionsplotter(String title){
        super(title);
        setSize(700,550);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        Canvas canvas = new Canvas();
        add(canvas);

        JButton tanButton = new JButton("tan(x)");
        //tanButton.setSize(50,25);
        add(tanButton);
        JButton powerButton = new JButton("x^2");
        //powerButton.setSize(50,25);
        add(powerButton);

        setVisible(true);
    }
}
