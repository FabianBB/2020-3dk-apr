public class TimeThread extends Thread{

    private TimeGUI timeGUI;

    public TimeThread(TimeGUI timeGUI) {
        this.timeGUI = timeGUI;
    }

    @Override
    public void run() {
        while (true){
            this.timeGUI.setTime();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
