import javax.swing.*;
import java.awt.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeGUI extends JFrame{

    private JLabel timelabel;

    private Color[] color = {Color.BLACK,Color.BLUE,Color.RED};
    private int currentColorPosition;

    public TimeGUI(){
        super("Uhrzeit anzeigen");
        setLayout(new FlowLayout());

        Thread thread = new ColorThread(this);
        JButton changeColorButton = new JButton("Farbwechsel starten");
        changeColorButton.addActionListener(e -> thread.start());

        timelabel = new JLabel(getTimeText());

        add(changeColorButton);
        add(timelabel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(300,80));
        setVisible(true);
        pack();
    }

    public void setColor(){
        currentColorPosition++;
        if (currentColorPosition>color.length-1){
            currentColorPosition = 0;
        }
        timelabel.setForeground(color[currentColorPosition]);
    }

    public void setTime(){
        timelabel.setText(getTimeText());
    }

    public String getTimeText(){
        return LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }
}
