public class ColorThread extends Thread{

    private TimeGUI timeGui;

    public ColorThread(TimeGUI timeGui) {
        this.timeGui = timeGui;
    }

    @Override
    public void run() {
        while (true) {
            timeGui.setColor();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
