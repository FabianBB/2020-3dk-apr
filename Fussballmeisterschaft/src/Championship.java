import java.util.ArrayList;
import java.util.List;

public class Championship {

    private final League league;
    private final List<Team> team;

    public Championship(League league) {
        this.league = league;
        this.team = new ArrayList<>();
    }

    public League getLeague() {
        return league;
    }

    public List<Team> getTeams() {
        return team;
    }

    public void addTeam(Team team){
        this.team.add(team);
    }

    @Override
    public String toString() {
        return "Championship:" + "league = " + league + "team = " + team;
    }
}
