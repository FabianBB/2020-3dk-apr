public class Game {

    private Team homeTeam;
    private Team awayTeam;
    private int scoreHome;
    private int scoreAway;
    private String date;

    public Game(Team homeTeam, Team awayTeam, String date) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.date = date;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public int getScoreHome() {
        return scoreHome;
    }

    public int getScoreAway() {
        return scoreAway;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Game:");
        sb.append("homeTeam = ").append(homeTeam.getName()).append("|");
        sb.append("awayTeam = ").append(awayTeam.getName()).append("|");
        sb.append("scoreHome = ").append(scoreHome).append("|");
        sb.append("scoreAway = ").append(scoreAway).append("|");
        sb.append("date = ").append(date);
        return sb.toString();
    }
}
