import java.sql.*;

public class ChampionDataAccess {

    private final String user;
    private final String pass;
    private final String connectionUrl;

    public ChampionDataAccess(String host, int port, String dbName, String user, String pass) {
        this.user = user;
        this.pass = pass;

        this.connectionUrl = "jdbc:mariadb://" + host + ":" + port + "/" + dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    private Connection connect() {
        System.out.println("Connecting to database");
        try {
            return DriverManager.getConnection(this.connectionUrl, this.user, this.pass);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection) {
        System.out.println("Closing connection");
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection: " + throwables.getMessage());
        }
    }

    public Championship loadForLeague(String name) {
        Connection connection = connect();
        Championship championship = null;
        String sql = """
                select c.id as champion_id, l.name as league_name, l.id as league_id, l.year as league_year, t.id as team_id, t.name as team_name
                from championship c
                    join team t on t.id = c.team
                    join liga l on c.league = l.id
                where l.name = ? 
                """;

        try {
            PreparedStatement prepStmt = connection.prepareStatement(sql);
            prepStmt.setString(1, name);
            ResultSet resultSet = prepStmt.executeQuery();
            while (resultSet.next()) {
                if (championship == null) {
                    int leagueId = resultSet.getInt("league_id");
                    String lName = resultSet.getString("league_name");
                    String year = resultSet.getString("league_year");
                    League l = new League(leagueId, lName, year);
                    championship = new Championship(l);
                    Team t = new Team(resultSet.getInt("team_id"), resultSet.getString("team_name"));
                    championship.addTeam(t);
                } else {
                    Team t = new Team(resultSet.getInt("team_id"), resultSet.getString("team_name"));
                    championship.addTeam(t);
                }
            }
            return championship;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not load championship", throwables);
        } finally {
            close(connection);
        }
    }


}
