import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GameDataAccess {

    private final String user;
    private final String pass;
    private final String connectionUrl;

    public GameDataAccess(String host, int port, String dbName, String user, String pass) {
        this.user = user;
        this.pass = pass;

        this.connectionUrl = "jdbc:mariadb://"+ host +":"+ port +"/"+ dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    private Connection connect(){
        System.out.println("Connecting to database");
        try {
            return DriverManager.getConnection(this.connectionUrl,this.user,this.pass);
        } catch (SQLException throwables){
            throw new RuntimeException("Could not connect to database",throwables);
        }
    }

    private void close(Connection connection){
        System.out.println("Closing connection");
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection: " + throwables.getMessage());
        }
    }

    public void insertGame(List<Game> game){
        Connection connection = connect();
        String sql = "insert into game(home_team,away_team,score_home,score_away,game_date) VALUES (?,?,null,null,?)";
        try {
            for (Game g:game) {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1,g.getHomeTeam().getId());
                preparedStatement.setInt(2,g.getAwayTeam().getId());
                preparedStatement.setString(3, g.getDate());
                preparedStatement.execute();
            }
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statment", throwables);
        } finally {
            close(connection);
        }
        System.out.println("Insert Execute successfully");
    }

    public List<Game> loadForGames() {
        Connection connection = connect();
        List<Game> gameList = new ArrayList<>();
        String sql = """
                select g.id as game_id, th.id as home_id, th.name as homeTeam, ta.id as away_id, ta.name as awayTeam, g.score_home as scoreHome, g.score_away as scoreAway ,g.game_date as match_date
                from game g
                    join team th on th.id = g.home_team
                    join team ta on ta.id = g.away_team
                """;

        try{
            PreparedStatement prepStmt = connection.prepareStatement(sql);
            ResultSet resultSet = prepStmt.executeQuery();
            while(resultSet.next()){
                Team h = new Team(resultSet.getInt("home_id"),resultSet.getString("homeTeam"));
                Team a = new Team(resultSet.getInt("away_id"),resultSet.getString("awayTeam"));

                String date = resultSet.getString("match_date");

                gameList.add(new Game(h,a,date));
            }
            return gameList;
        }catch(SQLException throwables){
            throw new RuntimeException("Could not load championship", throwables);
        } finally {
            close(connection);
        }
    }

}
