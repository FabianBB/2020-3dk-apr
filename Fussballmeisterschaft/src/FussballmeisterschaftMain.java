import java.util.ArrayList;
import java.util.List;

public class FussballmeisterschaftMain {

    public static void main(String[] args) {
        ChampionDataAccess champDAO = new ChampionDataAccess("localhost",3306,"fussballmeisterschaft","root","");
        printMeisterschaft(champDAO.loadForLeague("1. Klasse Waldviertel"));

        GameDataAccess gameDAO = new GameDataAccess("localhost",3306,"fussballmeisterschaft","root","");
        List<Game> games = createMatches(champDAO.loadForLeague("1. Klasse Waldviertel").getTeams());

        for (int i = 0; i < games.size()-1; i++) {
            System.out.println(games.get(i).getHomeTeam().getName()+" : "+games.get(i).getAwayTeam().getName());
        }

        //Adding Matches to the Database
        gameDAO.insertGame(games);

        //Loading Matches from the Database
        List<Game> loadedMatches = gameDAO.loadForGames();
        for (int i = 0; i < loadedMatches.size(); i++){
            System.out.println("Matches "+i+". :"+loadedMatches.get(i).getHomeTeam().getName()+" vs. "+loadedMatches.get(i).getAwayTeam().getName());
        }

    }

    static List<Game> createMatches(List<Team> teams){
        List<Game> games = new ArrayList<>();

        Team joker = teams.remove(teams.size()-1);

        for(int i = 0;i < teams.size();i++){
            int j = 0;
            while (j < teams.size()/2){
                games.add(new Game(teams.get(j),teams.get(teams.size()-1-j),"2021-06-17"));
                j++;
            }
            games.add(new Game(teams.get(j),joker,"2021-06-17"));

            Team t = teams.remove(0);
            teams.add(t);
        }
        return games;
    }

    static void printMeisterschaft(Championship meisterschaft) {
        League league = meisterschaft.getLeague();
        List<Team> teams = meisterschaft.getTeams();
        String headline = "---- " + league.getName() + " " + league.getYear() + " ----";
        System.out.println(headline);

        for (int i = 0; i < headline.length(); i++) {
            System.out.print("-");
        }
        System.out.print("\n");

        teams.forEach(team -> System.out.println(team.getId()+". "+team.getName()));
    }

}
